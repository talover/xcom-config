(function($) {
  $(document).ready(function(){
    $('.accordion__header').click(function(){
      $(this).toggleClass('accordion__header--active');
      $(this).next('.accordion__drop').slideToggle();
    });

    // .styler
    setTimeout(function() {
      $('.styler').styler({
        selectSearch: true,
      });
    }, 100);

    // .checkbox
    function checkboxActive(e) {
      var parents = e.parents('.checkbox');
      if(e.is(":checked")) {
        parents.addClass('checkbox--active');
      }else {
        parents.removeClass('checkbox--active');
      }
    }

    $('.checkbox__input').each(function(){
      checkboxActive($(this));
    });

    $('.checkbox__input').change(function(){
      checkboxActive($(this));
    });

    //

    $('.selected-slider').slick({
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      variableWidth: true,
      prevArrow: $('.selected-c__arrow--prev'),
      nextArrow: $('.selected-c__arrow--next')
    });

    // phone mask
    $('.phone-mask').inputmask({mask: "+7 (999) 999 99 99"});

    $('.phone-mask--val').keyup(function(){
      if ($(this).inputmask("isComplete")){
        $('#checkout').attr('class','btn btn--blue settings__total--btn');
      }else {
        $('#checkout').attr('class','btn btn--gray settings__total--btn');
      }
    });

    // modal
    $('.modal-btn').click(function (e) {
       e.preventDefault();
       var $this = $(this);
       $('.modal').removeClass('modal__in');
       $('.modal').css('top','50%');
       setTimeout(function () {
           // open new
           var scrollTop = $(window).scrollTop(),
               modal = $this.attr('href');

           if(window.innerWidth > 568) {
             $(modal).css('top',scrollTop + 100);
           }else {
             $(modal).css('top',scrollTop);
           }

           $(modal).addClass('modal__in');
           $('#overlay').fadeIn();
       },300);
   });

   $('.modal__close,#overlay, .modal__btn-close').click(function (e) {
       e.preventDefault();
       $('#overlay').fadeOut();
       $('.modal').removeClass('modal__in')
       $('.modal').css('top','50%');
   });

   // .header__search

   $('.header__search-btn').click(function(){
     $(this).toggleClass('header__search-btn--active');
     $('.header__search-cont').toggleClass('header__search-cont--active');
   });

   $('.header__search-input').keyup(function(){
     $('.header__search-result').addClass('header__search-result--active');
   });

   $(document).on('click',function(e){
     if(!$(e.target).closest('.header__search-input,.header__search-result').length) {
       $('.header__search-result').removeClass('header__search-result--active');
     }

     if(!$(e.target).closest('.header__search-btn,.header__search-cont').length) {
       $('.header__search-cont').removeClass('header__search-cont--active');
       $('.header__search-btn').removeClass('header__search-btn--active');
     }
   });

   // .aside__title

   $('.aside__title').click(function(){
     $(this).toggleClass('aside__title--active');
     $(this).next('.aside__cont').slideToggle();
   });
  });
})(jQuery);
